;;; multi-buffer.el --- multi everything!  -*- lexical-binding:t -*-

;; Copyright (C) 2024 Valentino Slavkin

;; Author: Valentino Slavkin R. <robot.slavkin@gmail.com>
;; Version: 0.2
;; Package-Requires: ((emacs "25.0"))
;; Keywords: frames, processes, terminals
;; URL: https://example.com/jrhacker/superfrobnicate

;;; Commentary:

;; Renames buffers like *eat* in order to be able to have
;; several of them. Inspired by multi-term and multi-vterm

;;; Code:

(defgroup multi-buffer nil "Multi-buffer group."
  :group 'convenience
  :version "0.1")

(defvar multi-buffer-alist nil
  "Here all the buffers modes and names are stored.")

(defcustom multi-buffer-format-strings '((eat-mode . "*eat %i*"))
  "Defines the used string per mode. Set as \='(mode . string).
Will be formatted using \='format-spec' and the \='multi-buffer-format-options'."
  :type '(list (cons symbol string))
  :group 'multi-buffer)

(defcustom multi-buffer-default-format "%i <%m>"
  "Default name for the buffer. Will be formatted using \='format-spec'."
  :type '(string))

(defun multi-buffer--add-to-alist (mode new-name)
  "Add the buffer to the alist \\='multi-buffer-alist' with MODE and NEW-NAME."
  (let* ((entry (assoc mode multi-buffer-alist)))
    (if entry
	(unless (member new-name (cdr entry))
	  (setcdr entry (append (cdr entry) `(,new-name)))) ;;The t is for appending at the end of the list
	(push (cons mode (list new-name)) multi-buffer-alist)
      )))

(defun multi-buffer--remove-from-alist (mode name)
  "Remove the buffer NAME from the \\='multi-buffer-alist', using MODE entry."
  (let* ((entry (assoc mode multi-buffer-alist))
	 (updated-list nil))
    ;; (message "entry %s" entry)
    (when entry
      (setq updated-list (remove name (cdr entry)))
      (setf (cdr entry) updated-list))))

(defun multi-buffer--format-name (mode index)
  "Format string using \\='format-spec'.
Uses \\='%m' as MODE and \\='%i' as the INDEX.
If the MODE has a \\='multi-buffer-format-strings' use that, else use the default."
  (let* ((fmat-list `((?m . ,(symbol-name mode))(?i . ,index)))
	 (fmat-str (cdr (assoc mode multi-buffer-format-strings))))
    (unless fmat-str
      (setq fmat-str multi-buffer-default-format))
    (format-spec fmat-str fmat-list)))

(defun multi-buffer--format-mode (mode)
  "Reformat all the buffers inside MODE alist.
If the buffer was killed, it removes it from the list"
  (let* ((entry (assoc mode multi-buffer-alist))
	 (index 0))
    (setcdr entry (remove nil (mapcar
		   (lambda (buffer)
		     (when (and buffer (get-buffer buffer))
		       (with-current-buffer buffer
			 (rename-buffer
			  (multi-buffer--format-name
			   mode
			   (setq index (+ 1 index)))))))
	  (cdr entry))))))

;;;###autoload
(defun multi-buffer-start ()
  "Set the name of the buffer, enabling multiple buffers."
  (interactive)
  (let* ((mode major-mode)
	 (new-name (multi-buffer--format-name mode (+ 1 (length (cdr (assoc mode multi-buffer-alist)))))))
    (rename-buffer new-name)
    (multi-buffer--add-to-alist mode new-name)
    ))

;;;###autoload
(defun multi-buffer-end (&optional new-name)
  "Remove the buffer from the list.
If recieves optional arg NEW-NAME renames the buffer."
  (interactive "B")
  (multi-buffer--remove-from-alist major-mode (buffer-name))
  (when current-prefix-arg (setq new-name (read-string "Insert the new name for the buffer")))
  (rename-buffer (if new-name new-name "multi-buffer placeholder")) ;; TODO This is horrible, will be replaced with the local variable, eventually.
  (multi-buffer--format-mode major-mode))

;;;###autoload
(define-minor-mode multi-buffer-mode
  "When activated, calls \='multi-buffer-start'.
When the buffer is killed, calls \='multi-buffer-end'"
  :lighter nil
  :keymap nil
  (if multi-buffer-mode
      (progn
	(multi-buffer-start)
	(add-hook 'kill-buffer-hook #'multi-buffer-end nil t))
    (remove-hook 'kill-buffer-hook #'multi-buffer-end t)))

(provide 'multi-buffer-mode)
;;; multi-buffer.el ends here
